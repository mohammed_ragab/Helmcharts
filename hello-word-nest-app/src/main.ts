import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const Port = process.env.PORT || 80
  console.log(`Listen to Port ${Port}`)
  await app.listen(Port);
}
bootstrap();
